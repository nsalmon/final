from typing import List, Tuple, Any


def change_colors(image: List[List[Tuple[int, int, int]]],
                  to_change: Tuple[int, int, int],
                  to_change_to: Tuple[int, int, int]) -> List[List[Tuple[int, int, int]]]:
    # Create a new image by copying the original image
    new_image: list[list[tuple[Any, Any, Any]]] = [[(r, g, b) for (r, g, b) in row] for row in image]

    # Change the color of each pixel in the new image
    for i in range(len(new_image)):
        for j in range(len(new_image[i])):
            if new_image[i][j] == to_change:
                new_image[i][j] = to_change_to

    return new_image


def rotate_right(image: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    # Rotate the image by 90 degrees to the right

    return [list(reversed(x)) for x in zip(*image)]


def mirror(image: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    image_mir = image
    image_mir.reverse()
    return image_mir


def rotate_colors(image: List[List[Tuple[int, int, int]]], increment: int) -> List[List[Tuple[int, int, int]]]:
    # Rotate the colors of the image by the given increment
    rotated_image = image.copy()

    for row in rotated_image:
        for pixel in row:
            for i in range(3):
                pixel[i] = (pixel[i] + increment) % 256

    return rotated_image


def blur(image: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    # Apply a blur effect to the image
    blurred_image = image.copy()

    row: int
    for row in range(1, len(image) - 1):
        for col in range(1, len(image[row]) - 1):
            pixel = image[row][col]
            new_pixel = [0, 0, 0]

            for i in range(-1, 2):
                for j in range(-1, 2):
                    new_pixel[0] += image[row + i][col + j][0]
                    new_pixel[1] += image[row + i][col + j][1]
                    new_pixel[2] += image[row + i][col + j][2]

            new_pixel[0] //= 9
            new_pixel[1] //= 9
            new_pixel[2] //= 9

            blurred_image[row][col] = tuple(new_pixel)

    return blurred_image


def shift(image: List[List[Tuple[int, int, int]]], horizontal_shift: int, vertical_shift: int) -> List[
    List[Tuple[int, int, int]]]:
    # Shift the image horizontally and vertically
    shifted_image = image.copy()

    height = len(image)
    width = len(image[0])

    # Create a new empty image to store the shifted image
    new_image = [[(0, 0, 0)] * width for _ in range(height)]

    # Shift the image horizontally
    for row in range(height):
        for col in range(width):
            new_row = (row + vertical_shift) % height
            new_col = (col + horizontal_shift) % width
            new_image[new_row][new_col] = image[row][col]

    # Copy the new image back to the original shifted_image
    for row in range(height):
        for col in range(width):
            shifted_image[row][col] = new_image[row][col]

    return shifted_image


def crop(image: List[List[Tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> List[
    List[Tuple[int, int, int]]]:
    # Crop the image to a specified rectangle
    cropped_image = image.copy()

    # Calculate the dimensions of the cropped image
    right = x + width
    bottom = y + height

    # Create a new empty image to store the cropped image
    new_image = [[(0, 0, 0)] * len(image[0]) for _ in range(len(image))]

    # Crop the image
    for row in range(len(image)):
        for col in range(len(image[row])):
            if x <= col < right and y <= row < bottom:
                new_image[row][col] = image[row][col]

    return new_image


def grayscale(image: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    # Convert the image to greyscale
    new_image = [[(0, 0, 0)] * len(image[0]) for _ in range(len(image))]

    # Convert each pixel to greyscale
    for row in range(len(image)):
        for col in range(len(image[row])):
            r, g, b = image[row][col]
            grayscale_value = (r + g + b) // 3
            new_image[row][col] = (grayscale_value, grayscale_value, grayscale_value)

    return new_image


def filter(image: List[List[Tuple[int, int, int]]], r_multiplier: int, g_multiplier: int, b_multiplier: int) -> List[
    List[Tuple[int, int, int]]]:
    # Apply the filter to the image
    new_image = [[(0, 0, 0)] * len(image[0]) for _ in range(len(image))]

    # Apply the filter to each pixel
    for row in range(len(image)):
        for col in range(len(image[row])):
            r, g, b = image[row][col]
            new_r = min(255, r * r_multiplier)
            new_g = min(255, g * g_multiplier)
            new_b = min(255, b * b_multiplier)
            new_image[row][col] = (new_r, new_g, new_b)

    return new_image
