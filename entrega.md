NEREA SALMÓN GAMO
n.salmon.2023@alumnos.urjc.es
Video: https://youtu.be/ZrRZ-QkFGvs

Requisitos minimos: 
-Método change_color
-Método rotate_right
-Método mirror
-Método rotate_colors
-Método shift
-Método crop
-Método grayscale
-Método filter
-Programa trnsform_simple.py
-Programa trnsform_args.py
-Programa trnsform_multi.py